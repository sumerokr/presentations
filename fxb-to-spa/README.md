FXB to SPA

  Curent solution
    Overview
      Traditional website
      req, res schema
      Per page navigation
    Codebase
      All in one place
      BE based templates (binded to BE data and BE technology stack)
      BE devs create busines logic and templates

  Proposal
    Overview
      Examples
      Application like behaviour
      req, res schema
      Framed navigation
    Codebase
      Separated Front-end and Back-end
      BE devs care about data, create API
      FE devs care about templates (UI)

  Comparison
    Traditional website PROS
      SEO friendly
        Server render
      Initial load speed
        No Front-end framework to download, native navigation
    Traditional website CONS
      Data is not reusable
        Front-end and Back-end codebases are tightly coupled
      Second-rate code quality
        Messed responsibility of developers
      Slow runtime performance
        Need to reload every page again and again

    SPA Pros
      Fast runtime performance
        No need to reload common page layout
      Better code quality
        Separated responsibility (FE / BE)
      Reusable data
        Back-end API can be used for any other service
    SPA Cons
      SEO unfriendly
        Templates compiled on the client side
      Initial load speed
        JS framework takes some time to load

picture of FE - BE meme
