JS framework definition
  Collection of components and libraries (mostly interdependent) to fulfill the needs of a single page web application

  Usualy it means:
    - routing between pages
    - networking with back-end
    - interaction with DOM
    - standard data flow approach

Learning curve
  How easy is a framework to learn?
    React official documentation offer a couple of Getting Started guides that are decently written and give a decent walk-through to newcomers, but it's not as clear and organized as the official documentation for Vue. The official documentation for Vue is very well written, and covers just everything a developer would face while developing Vue apps. Vue is more tightly defined than React; that also means it is more opinionated. For Vue many questions are answered straight in the documentation itself, not needing to search too much in other places.
Code style
  JSX, which is a way of writing HTML within JavaScript code.
  Vue templates is a way of writing JavaScript within HTML code.
  render() {
    return (
      <ul>
      {
        data.map(item => {
          return <li key={item.id}>{item.title}</li>
        });
      }
      </ul>
    );
  }

  <ul>
    <li v-for="item in data">
      {{ item.title }}
    </li>
  </ul>
Performance
  How performant are apps built with the framework?
    https://www.stefankrause.net/js-frameworks-benchmark7/table.html
  How big is the footprint?
    Vue 60.2kB MINIFIED 21.4kB GZIPPED
    React 96.2kB MINIFIED 31.6kB GZIPPED
Support
  React
    React is a framework founded and maintained by Facebook.
    The React team size at Facebook is said to include 10 dedicated developers.
    React doesn’t have an actual roadmap, but work based on RFCs
  Vue
    Vue is an independent library, founded by Evan You.He also manages the maintenance of Vue and its roadmap ahead.
    The Vue team size includes 23 dedicated developers.
    Vue has a high-level roadmap
Ecosystem
  Vue’s companion libraries for state management and routing (among other concerns) are all officially supported and kept up-to-date with the core library. React instead chooses to leave these concerns to the community, creating a more fragmented ecosystem. Being more popular though, React’s ecosystem is considerably richer than Vue’s.
  React: Redux, Router (3rd party)
    Redux 6.9kB MINIFIED 2.5kB GZIPPED
    Router 30.4kB MINIFIED 8kB GZIPPED
    React: 64921 npm packages, 2.6M downloads/m
  Vue: Vuex, Router (officially)
    Vuex 9.6kB MINIFIED 2.9kB GZIPPED
    Router 23.7kB MINIFIED 8.2kB GZIPPED
    Vue: 17092 npm packages, 0.5M downloads/m
Community
  React: 100000 SO tags, 111k GH stars
  Vue: 32000 SO tags, 115k GH stars
native
  React: React Native
  Vue: Native Script
Hiring
  Being the most popular framework out there these days, React has an advantage if you’re in the market for React developers.

  Vue is the new “hotness” in the front-end industry.
  These days, it is not uncommon to find developers who have some experience with Vue.
satisfaction
  I've never heard of it 0 0 || 23 5
  I've HEARD of it, and am NOT interested 11 11 || 34 21
  I've HEARD of it, and WOULD like to learn it 32 27 || 33 51
  I've USED it before, and would NOT use it again 5 4 || 1 2
  I've USED it before, and WOULD use it again 53 58 || 10 20
trend

summary
React
  Pros
    Industry standard.
    Skilling FEDs with the most popular framework out there.
    Easier hiring of strong FEDs.
    More secure future and stability, based on a stable past and a strong backing company.
    More significant community, a plethora of tools and packages.
    Web and mobile apps can share some code.
  Cons
    Keeping the division between FEDs and BEDs; React requires to learn a lot to become an expert.
    Would require much more time to train devs.
    Deliver slower (at least for the initial heavy lifting).
Vue
  Pros
    Vue’s core modules (Vuex, Router, etc.) are built-in and work fantastic.
    Choosing the “next” thing, and not the “current.”
    Being more unique; leading the herd and not following it.
    Much faster ramp-up phase, both FEDs and BEDs would feel natural in Vue code, fast.
    Better promotes Full-Stack culture; allows cross-product development.
  Cons
    Stepping in a more experimental land, not risky, but edgy.
    More difficult to find experienced Vue devs.
    Fewer plugins and tools available, smaller community.
    Not being React, devs don’t gain experience with the most popular framework today.
