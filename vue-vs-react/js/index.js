window.Reveal.initialize({
  // width: 1000,
  dependencies: [
    {
      src: '../node_modules/reveal.js/plugin/highlight/highlight.js',
      async: true,
      callback: function () {
        window.hljs.initHighlightingOnLoad()
      }
    },
    {
      src: '../node_modules/reveal.js/plugin/notes/notes.js',
      async: true
    }
  ]
})
